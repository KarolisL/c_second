#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#define BUFFLEN 1024

#define CT_PUNCTUATION 0
#define CT_SPACE 1
#define CT_ALNUM 2
#define CT_UNPRINTABLE 3



char *chomp(char *str);
int charTypeOf(char c);


/* argv[1] – input filename
 * argv[2] – output filename
 */
int main(int argc, char *argv[])
{
    FILE *in, *out;
    /* file-level index of current character */
    long long index = 0; 
    long long punctuation_index = -1;
    long long word_index = -1;
    long long punctuations_sum = 0;
    long long punctuations = 0;
    long long word_lens = 0;
    long long word_count = 0;
    bool in_word = false

    int i;
    char line[BUFFLEN];

    if(argc != 3) {
        fprintf(stderr, "Usage: %s <input_file> <output_file>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if(!(in = fopen(argv[1], "r"))) {
        fprintf(stderr, "Unable to open \"%s\" file for reading.\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    while(fgets(line, BUFFLEN, in) != NULL) {
        for(i = 0; i < strlen(chomp(line)); ++i, ++index) {
            switch(charTypeOf(line[i])) {
            case CT_PUNCTUATION: 
                if(punctuation_index != -1) {
                    punctuations_sum += index - punctuation_index;
                    punctuations++;
                }
                punctuation_index = index;
            case CT_SPACE:
                if(inWord) {
                    inWord = false;
                    word_lens += 
                break;
            case CT_UNPRINTABLE:
                    break;
            case CT_ALNUM:
                if(!in_word) {
                    word_index = index;
                    in_word = true;
                }
            }
        }
    }
}

char *chomp(char *str) {
    size_t len = strlen(str);
    if(ptr[len - 1] == '\n') {
        ptr[len - 1] = '\0'
    }
    return ptr;
}

int type_of_char(char c) {
    if(isalnum(c))
        return CT_ALNUM;
    else if (isblank(c))
        return CT_SPACE;
    else if(!isprint(c))
        return CT_UNPRINTABLE;
    else
        return CT_PUNCTUATION;
}
